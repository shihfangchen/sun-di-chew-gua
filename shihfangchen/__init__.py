from __future__ import absolute_import
import os
from shihfangchen.feat_extract import parse_audio_files, parse_audio_file
import numpy as np
import shihfangchen.models
from keras.utils import to_categorical
from keras.optimizers import SGD, Adam,Adamax,Nadam,AdaBound
#from models import svm, nn, cnn
#from shihfangchen.adabound.adabound import AdaBound

from keras import backend as K
from keras.optimizers import Optimizer


import keras
import matplotlib.pyplot as plt
class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = {'batch':[], 'epoch':[]}
        self.accuracy = {'batch':[], 'epoch':[]}
        self.val_loss = {'batch':[], 'epoch':[]}
        self.val_acc = {'batch':[], 'epoch':[]}

    def on_batch_end(self, batch, logs={}):
        self.losses['batch'].append(logs.get('loss'))
        self.accuracy['batch'].append(logs.get('acc'))
        self.val_loss['batch'].append(logs.get('val_loss'))
        self.val_acc['batch'].append(logs.get('val_acc'))
        
    def on_epoch_end(self, batch, logs={}):
        self.losses['epoch'].append(logs.get('loss'))
        self.accuracy['epoch'].append(logs.get('acc'))
        self.val_loss['epoch'].append(logs.get('val_loss'))
        self.val_acc['epoch'].append(logs.get('val_acc'))
        
    def loss_plot(self, loss_type):
        iters = range(len(self.losses[loss_type]))
        plt.figure()
        # acc
        plt.plot(iters, self.accuracy[loss_type], 'r', label='train acc')
        # loss
        plt.plot(iters, self.losses[loss_type], 'g', label='train loss')
        if loss_type == 'epoch':
            # val_acc
            plt.plot(iters, self.val_acc[loss_type], 'b', label='val acc')
            # val_loss
            plt.plot(iters, self.val_loss[loss_type], 'k', label='val loss')
        plt.grid(True)
        plt.xlabel(loss_type)
        plt.ylabel('acc-loss')
        plt.legend(loc="upper right")
        plt.show()
				  

def feature_extraction(data_path):
    """Parses audio files in supplied data path.
    -*- author: mtobeiyf https://github.com/mtobeiyf/audio-classification -*-
    """
    r = os.listdir(data_path)
    r.sort()
    features, labels = parse_audio_files(data_path, r)
    return features, labels

def train(features, labels, type='cnn', num_classes=None, print_summary=True,
    save_model=False, lr=0.01, loss_type=None, epochs=50, 
	#optimizer='SGD', 
	#optimizer='Adam', 
	#optimizer='Adamax', 
	#optimizer='Nadam', 
	optimizer='AdaBound', 
	

	verbose=True):
	
    """Trains model based on provided feature & target data
    Options:
    - epochs: The number of iterations. Default is 50.
    - lr: Learning rate. Increase to speed up training time, decrease to get more accurate results (if your loss is 'jumping'). Default is 0.01.
    - optimiser: Default is 'SGD'.
    - print_summary: Prints a summary of the model you'll be training. Default is False.
    - loss_type: Classification type. Default is categorical for >2 classes, and binary otherwise.
    """
    #print("\n",'1 train Hello World')  
    
    labels = labels.ravel()
    if num_classes == None: num_classes = np.max(labels, axis=0)
    print("\n",'num_classes',num_classes,"\n")  
    model = getattr(models, type)(num_classes)
    if print_summary == True: model.summary()

    if loss_type == None:
        loss_type = 'binary' if num_classes <= 2 else 'categorical'
 
    model.compile(
	#optimizer=SGD(lr=lr),
	#optimizer=SGD(lr=lr, decay=lr/epochs, momentum=0.9, nesterov=True),
	#optimizer=Adam(lr=lr,decay=lr/epochs),
	#optimizer=Adam(lr=lr),
	#optimizer=Adamax(lr=lr),
	#optimizer=Nadam(lr=lr),loss='%s_crossentropy' % loss_type,metrics=['accuracy'])
	
		       optimizer = AdaBound(lr=1e-04,#0.0001
					final_lr=0.5,
					gamma=1e-03,
					weight_decay=0.,
					amsbound=True),
				 loss='%s_crossentropy' % loss_type,	
                 metrics=['accuracy']) 
	

    if loss_type == 'categorical':
        y = to_categorical(labels - 1, num_classes=num_classes)
    else:
        y = labels - 1

    x = np.expand_dims(features, axis=2)
	#batch_size=64
    history = LossHistory()
    model.fit(x, y, batch_size=64, epochs=epochs, verbose=verbose,callbacks=[history])
    history.loss_plot('epoch')
    return model

def predict(model, data_path):
    """Trains model based on provided feature & target data
    Options:
    - epochs: The number of iterations. Default is 50.
    - lr: Learning rate. Increase to speed up training time, decrease to get more accurate results (if your loss is 'jumping'). Default is 0.01.
    - optimiser: Default is 'SGD'.
    - print_summary: Prints a summary of the model you'll be training. Default is False.
    - type: Classification type. Default is categorical for >2 classes, and binary otherwise.
    """
    #print("\n",'predict',"\n")  
    x_data = parse_audio_file(data_path)
    X_train = np.expand_dims(x_data, axis=2)
    pred = model.predict(X_train)
    return pred

def print_leaderboard(pred, data_path):
    #print("\n",'pyaudioclassification2',"\n")  
    """Pretty prints leaderboard of top matches
    """
    r = os.listdir(data_path)
    r.sort()
    sorted = np.argsort(pred)
    count = 0
    #print (r)
    for index in (-pred).argsort()[0]:
        print('%d.' % (count + 1), r[index + 1], str(round(pred[0][index]*100)) + '%', '(index %s)' % index)
        #count += 1
        break
