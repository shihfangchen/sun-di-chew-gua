import numpy as np
import os
from shihfangchen import feature_extraction, train, predict, print_leaderboard

parent_dir = '.'

type1 = "./feat.npy"
type2 = "./label.npy"
# step 1: preprocessing
if np.DataSource().exists(type1) and np.DataSource().exists(type2):
    features, labels = np.load(type1), np.load(type2)
else:
    features, labels = feature_extraction('./data_denoiseANDrow_wav/')
    np.save('./feat.npy', features)
    np.save('./label.npy', labels)

# step 2: training
if np.DataSource().exists("./model_amsbound_1000_BN.h5"):
    from keras.models import load_model
    model = load_model('./model_amsbound_1000_BN.h5')
else:
    model = train(features, labels, epochs=1000)
    model.save('./model_amsbound_1000_BN.h5')

# step 3: prediction
count = 0
data = os.listdir('test/')
fp = open("result.txt", 'w')
for file in data:
	#denoise(file)
    pred = predict(model, './test/' + file)
	
    r = os.listdir('./data_denoiseANDrow_wav/')
    r.sort()
    sorted = np.argsort(pred)
    #print (r)
    for index in (-pred).argsort()[0]:
        #print(index)
        #print(r[index + 1], str(round(pred[0][index]*100)) + '%', '(index %s)' % index)
        #print(r[index + 1] + '-' + file)
        print('ans: ' + r[index + 1] + ', file: ' + file)
        fp.write('ans: ' + r[index + 1] + ', file: ' + file + '\n')
        break
	#print_leaderboard(pred, './data_denoiseANDrow_wav/')
	#print(file)
fp.close()